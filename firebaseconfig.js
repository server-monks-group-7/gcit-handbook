// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';

const firebaseConfig = {
    apiKey: "AIzaSyDjom4J_ZVPSYTFe_a4d1OXU9PlFwgU-JY",
    authDomain: "gcit-handbook.firebaseapp.com",
    projectId: "gcit-handbook",
    storageBucket: "gcit-handbook.appspot.com",
    messagingSenderId: "273081966135",
    appId: "1:273081966135:web:7791eeffe52511fd5c69b2",
    measurementId: "G-Z21XRRS3M7"
  }

  if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
  }
  const auth=firebase.auth();

  export{firebase,auth};