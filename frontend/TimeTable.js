import { StyleSheet, Text, View , TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'

const TimeTable = () => {
    const navigation=useNavigation();
    return (
    <View style={styles.TimeTableContainer}>
         <View style={styles.gcit}>
            <Image
                style={styles.gcit}
                source={require('../images/gcit.logo.jpg')}
            />
        </View>
        <Text style={styles.heading}>Courses</Text>
        <View style={{marginTop:70,}}>
             <View style={styles.courseContainer}>
                <TouchableOpacity
                     style={styles.courses}
                     onPress={()=>navigation.navigate('Digital Media')}
                >
                    <Text style={styles.courseText}>Digital Media And Development</Text>
                </TouchableOpacity>

                <TouchableOpacity
                     style={styles.courses}
                     onPress={() => navigation.navigate('AI and DS')}
                >
                    <Text style={styles.courseText}>AI and Data Science</Text>
                </TouchableOpacity>
             </View>

            <View style={styles.courseContainer}>
               <TouchableOpacity
                     style={styles.courses}
                     onPress={() => navigation.navigate('Blockchain')}
                >
                    <Text style={styles.courseText}>Blockchain Development</Text>
                </TouchableOpacity>

                <TouchableOpacity
                     style={styles.courses}
                     onPress={() => navigation.navigate('Full Stack')}
                >
                    <Text style={styles.courseText}>Full Stack Development</Text>
                </TouchableOpacity>

            </View>
            
            <View style={styles.courseContainer}>
                
                <View style={{backgroundColor:'rgba(44,126,249,1)', margin:10, borderRadius:10, alignItems:'center'}}>
                    <Text style={styles.courseText1}>Bsc. CS</Text>
                    <View style={{display:'flex', flexDirection:'column'}}>
                                    <TouchableOpacity 
                                        style={styles.years}
                                        onPress={() => navigation.navigate("Third Year CS")}
                                    >
                                        <Text style={styles.yearText}>3rd Year</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity 
                                        style={styles.years}
                                        onPress={() =>navigation.navigate("Fourth Year CS")}
                                    >
                                        <Text style={styles.yearText}>4th Year</Text>
                                    </TouchableOpacity>
                    </View>
                </View>
                    

                <View style={{backgroundColor:'rgba(44,126,249,1)', margin:10, borderRadius:10, alignItems:'center'}}>
                    <Text style={styles.courseText1}>Bsc. IT</Text>
                    <View style={{display:'flex', flexDirection:'column'}}>
                                    <TouchableOpacity 
                                        style={styles.years}
                                        onPress={() => navigation.navigate('Third Year IT')}
                                    >
                                        <Text style={styles.yearText}>3rd Year</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity 
                                        style={styles.years}
                                        onPress={() => navigation.navigate('Fourth Year IT')}
                                    
                                    >
                                        <Text style={styles.yearText}>4th Year</Text>
                                    </TouchableOpacity>
                    </View>
                </View>
            </View>
            
                
        </View>
    </View>
  )
}

export default TimeTable;

const styles = StyleSheet.create({
    TimeTableContainer:{
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center',
    },
    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
    },
    heading:{
        fontFamily:'serif',
        fontSize:34,
        fontWeight:'bold',
        marginTop:35,
    },
    courseContainer:{
        display:'flex',
        flexDirection:'row',
    },
    courses:{
        width:170,
        borderColor:'black',
        borderWidth:1,
        margin:10,
        alignItems:'center',
        borderRadius:10,
        backgroundColor:'rgba(44,126,249,1)',
    },
    years:{
        width:170,
        borderColor:'black',
        borderWidth:1,
        alignItems:'center',
        borderRadius:10,
        marginBottom:10,
        backgroundColor:'white',
    },
    yearText:{
        fontSize:16,
        fontFamily:'serif'
    },
    courseText:{
        fontFamily:'serif',
        fontSize:18,
        fontWeight:'bold',
        color:'white',
        padding:20,
    },
    courseText1:{
        fontFamily:'serif',
        fontSize:18,
        fontWeight:'bold',
        color:'white',
        padding:10,
    },
    yearContainer:{
        display:'flex',
        flexDirection:'column',
        borderWidth:1,
        width:170,
    }
})