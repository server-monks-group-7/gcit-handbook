import React from "react";
import { Image,StyleSheet, View } from "react-native";

const collegeLogo=()=>{
    return(
        <View style={styles.gcit}>
            <Image
                style={styles.gcit}
                source={require('../images/gcit.logo.jpg')}
            />
        </View>
    )
}

const styles=StyleSheet.create({
    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
    }
});
export default collegeLogo;
