import { StyleSheet, Text, View,Image,ScrollView,TouchableOpacity } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'

const Rules = () => {
    const navigation=useNavigation();
  return (
    <View style={styles.rulesContainer}>
      <View style={styles.gcit}>
            <Image
                style={styles.gcit}
                source={require('../images/gcit.logo.png')}
            />
        </View>
        <Text style={styles.heading}>Rules and Regulation</Text>

        <ScrollView>
            <View style={styles.scrollContainer}>
                <View style={{width:350, alignItems:'center'}}><Text style={{fontFamily:'serif', fontSize:20,fontWeight:'bold', margin:10,}}>Contents</Text></View>
                <TouchableOpacity
                onPress={() => navigation.navigate("General Information")}
                ><Text style={{fontFamily:'serif', fontWeight:'bold',padding:5, color:'blue'}}>1. General Information</Text></TouchableOpacity>
                <Text style={{fontFamily:'serif',padding:10, lineHeight:25}}> 
                1.1 Rights and Responsibility
                {'\n'}
                1.2 VISION and MISSION 
                </Text>
                <TouchableOpacity
                onPress={() => navigation.navigate("Academic Information")}
                >
                <Text style={{fontFamily:'serif', fontWeight:'bold', padding:5,color:'blue'}}>2. Academic Information</Text>
                </TouchableOpacity>
                <Text style={{fontFamily:'serif',padding:10, lineHeight:25}}> 
                2.1 Assessment and Progression Rule 
                {'\n'}
                2.2 Assessment of a Module and Progression 
                {'\n'}
                2.3 Re-assessment and Repeat of a module 
                {'\n'}
                2.4 Award of Grades 
                {'\n'}
                2.5 Requirements for Graduation 
                </Text>
                <TouchableOpacity
                onPress={() => navigation.navigate('Examination Guidelines')}
                >
                <Text style={{fontFamily:'serif', fontWeight:'bold', padding:5, color:'blue'}}>3. Examination Guidelines</Text>
                </TouchableOpacity>
                <Text style={{fontFamily:'serif',padding:10, lineHeight:25}}>
                3.1 Examinations 
                {'\n'}
                3.2 Responsibility to Attend Exam (for candidates)
                {'\n'}
                3.3 Use of Material and Aid 
                {'\n'}
                3.4 Before the Examination 
                </Text>
                <TouchableOpacity
                onPress={() => navigation.navigate('Attendance')}
                >
                <Text style={{fontFamily:'serif', fontWeight:'bold', padding:5,color:'blue'}}>4. Attendance</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => navigation.navigate("Student Services")}
                >
                <Text style={{fontFamily:'serif', fontWeight:'bold', padding:5,color:'blue'}}>5. Student Services</Text>
                </TouchableOpacity>
                <Text style={{fontFamily:'serif',padding:10, lineHeight:25}}>
                5.1 Residence or Hostel Accommodation
                {'\n'}
                5.2 Student Mess 
                {'\n'}
                5.3 Hostel Resident Tutors/Resident Tutor 
                {'\n'}
                5.4 Mobile phones/ Paging Devices, and Other Electronic Devices 
                {'\n'}
                5.5 Academic Block and office 
                {'\n'}
                5.6 Dress Code 
                {'\n'}
                5.7 Computing Facilities 
                {'\n'}
                5.8 Class Tutorial Rooms 
                {'\n'}
                5.9 Library 
                {'\n'}
                5.10 Prayer Sessions 
                {'\n'}
                5.11 Games and Sports Facilities 
                {'\n'}
                5.12 Morning Assembly 
                {'\n'}
                5.13 Consultation and Counseling 
                {'\n'}
                5.14 Outstation Leave 
                {'\n'}

                </Text>
            </View>
        </ScrollView>
    </View>
  )
}

export default Rules

const styles = StyleSheet.create({
    rulesContainer:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center',
        justifyContent:'center'
        },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:3,
        marginBottom:20,
   },
    heading:{
        fontSize:28,
        fontFamily:'serif',
        fontWeight:'bold',
        marginBottom:10,
    },
    scrollContainer:{
        marginTop:10,
        borderWidth:1,
        width:350,
        borderRadius:10,
    }
})