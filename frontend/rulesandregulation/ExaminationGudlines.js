import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'
import React from 'react'

const ExaminationGuidelines = () => {
  return (
    <View style={styles.giPage}>
      <View style={styles.gcit}>
         <Image
             style={styles.gcit}
             source={require('../../images/gcit.logo.png')}
         />
     </View>
     <View style={{alignItems:'center'}}><Text style={styles.heading}>Examination Guidelines</Text></View>
     
     <ScrollView style={styles.scrollContainer}>
            {/* <View style={styles.scrollContainer}> */}
            <Text style={styles.subheading}>1. Examination</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            • Students enrolled with the college will have semester
            examinations, except in few modules. Students are expected
            to complete the programme the total time span of six years for
            honors and five years for general degree. Students are required
            to qualify for each semester in continuous assessments and
            examinations separately. Revaluation is not allowed in the
            programme. 
            </Text>

            <Text style={styles.subheading}>2. Responsibility to Attend Exam</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            • Candidates are responsible for checking the dates,
                times and locations of their examinations from the
                examination schedules, and for presenting themselves
                for examination at the appointed place and time. Exami
                nation schedules will be announced /displayed by the
                Board of Examiners approximately three weeks prior to
                the first day of the examination.
                {'\n'}
            • Where all the part of the assessment for an examination
            is by means other than of a formally invigilated written
            examination, the module tutor will announce details of
            appropriate arrangements, and it is the candidate’s re
            sponsibility to acquaint themselves with these details.
            {'\n'}
            • A candidate who is unable to attend an examination
            under normal conditions because of illness or any other
            extenuating circumstances, or who would be
            significantly disadvantaged if required to do so, may be
            permitted to sit for the examination under special
            conditions. Such special arrangements which may
            include (if necessary) additional time, a re-scheduling
            of the examination, the use of dictating or other facilities,
            and/or the use of premises outside the examination hall,
            will be approved in advance by the Board of Examiners.
            In case of disability, the assessment regulation
            governing assessment will apply. Candidates
            requesting special arrangements on medical grounds
            will be requested to substantiate their request with
            a medical certificate or evidential documents.
            {'\n'}
            • A candidate is requested to notify the Board of Examiner
            in writing immediately if for any reason he/she is unable
            to sit for an examination because of illness, accident or
            other causes arising immediately prior to or during the
            examination. The candidate’s letter must state clearly
            the examination in question, the cause of the absence
            or inability, and must enclose the relevant supporting
            documents for evidence (e.g. medical certificates)
            </Text>

            <Text style={styles.subheading}>3. Use of Material and Aid</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
             •  Candidates will provide themselves with the necessary
                writing and drawing tools.
                {'\n'}
            •  All questions in a written examination must be answered
            using only answer booklets, supplementary sheets and
            other materials and aids provided by the Board for that
            examination. Candidates at any examination, either
            written or practical, will not be permitted to have on their
            possession or to make use of any paper, books, notes,
            dictionaries, instruments, aids or other materials unless
            expressly authorized in the rubric of the examination
            paper. Authorized materials and aids will be subject to
            inspection by the invigilator.
            {'\n'}
            •  Where simple or scientific electronic calculators are
            permitted for use in an examination, these instruments
            should be non-programmable unless expressly allowed,
            hand-held, self-powered, and silent in operation. Candi
            dates are responsible in ensuring that their calculators
            are in working order, and have a sufficient power supply,
            and that alternative means of calculating are available in
            the event that their electronic calculators fail during an
            examination.
            {'\n'}
            •  Mathematical tables and all other materials provided by
            the Board of Examiners for use in examinations must
            not be removed by candidates from the examination
            venue.
            </Text>

            <Text style={styles.subheading}>4.Before the Examination</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            • Candidates are required to bring along their student ID
            card/ examination entry card whenever they have an ex
            amination for verification purpose. Candidates who are
            unable to present their cards will not be allowed to write
            the examination.
            {'\n'}
            • Invigilators should ensure that only examination candi
            dates and authorized examiners for the day are allowed
            to enter the examination venue.
            {'\n'}
            • Students should check their seat numbers, if any
            student cannot find his/her number/name on the seating
            plan; he/she should inform the invigilator once admitted
            into the examination venue.
            {'\n'}
            • Candidates will be admitted into the examination venue
            at least 10 minutes before the commencement of an
            examination. Candidates who arrive late and are
            admitted will not be given extra time.
            </Text>
        </ScrollView>
    </View>
  )
}

export default ExaminationGuidelines

const styles = StyleSheet.create({
    giPage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        alignItems:'center',
        alignContent:'center',
        },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:20,
       },
    scrollContainer:{
        marginTop:10,
        borderWidth:1,
        width:350,
        borderRadius:10,
        height:600,
        borderColor:'blue'
    },
    heading:{
        fontFamily:'serif',
        fontSize:20,
        fontWeight:'bold',
        padding:10,
    },
    subheading:{
        fontFamily:'serif',
        fontSize:16,
        fontWeight:'bold',
        padding:10,
        lineHeight:30
    }
})