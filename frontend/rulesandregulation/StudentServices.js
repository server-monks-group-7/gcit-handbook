import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'
import React from 'react'

const StudentServices = () => {
  return (
    <View style={styles.giPage}>
      <View style={styles.gcit}>
         <Image
             style={styles.gcit}
             source={require('../../images/gcit.logo.png')}
         />
     </View>
     <View style={{alignItems:'center'}}><Text style={styles.heading}>Student Services</Text></View>
     
     <ScrollView style={styles.scrollContainer}>
            {/* <View style={styles.scrollContainer}> */}
            <Text style={styles.subheading}>1.Residence or Hostel Accommodation</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            Students who are admitted on merit basis with full scholarship
            shall be provided the hostel accommodation. A monthly rent of
            Nu. 150/- per student from July 2017 shall be deducted from
            your monthly stipend of Nu. 1500/-.As a resident of the hostel/
            college you are required to:
            {'\n'}
            (a) Help maintain an atmosphere of peace and safety of all
            hostel/college residents.
            {'\n'}
            (b) Participate in social works in the areas allotted by your
            respective councilors and Resident Tutorss.
            {'\n'}
            (c) Cooperate with the officials to help keep the areas clean
            and hygienic for healthy living.
            {'\n'}
            (d) Respect all the basic human principles of healthy living
            such as adhering to the norms established.
            {'\n'}
            (e) Take over the quarters and other facilities in writing.
            (f) Return the quarter, furniture and facilities before leaving
            the hostel or be accountable.
            {'\n'}
            (g) Report to your respective Resident Tutors or councilor if
            any problems are observed by any of the occupants.
            {'\n'}
            To retain the privilege, you are required to pass every semester examinations. Repeaters will have to arrange their own accommodation and
            other living expenses (College will provide accommodation if available).
            Anyone abusing and damaging the college properties shall be subject
            to disciplinary actions. Residents will need to replace any item damaged
            after occupation and bear the cost of power consumption beyond the
            specific limit for different quarters/hostels. 
            </Text>

            <Text style={styles.subheading}>2. Student Mess</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            Residents have to avail ‘Common Mess’ facility. Common Mess is the
            mess facilities provided by the college. You can avail this facility during
            your entire stay in the college. Moreover, it’s mandatory for all the first
            year residents to take part in the Common Mess at least for two semesters for administrative reasons.{'\n'}
            A mess bill of Nu. 1200/- per head per month shall be charged from the
            common mess participants. This is subject to review from time to time
            depending on the economic conditions and food quality requirements
            
            </Text>

            <Text style={styles.subheading}>3. Hostel Resident / Tutor Resident</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            The immediate person to contact for any problems and queries in the
            hostel premises are the hostel Resident Tutorss and councilors. They
            will supervise, counsel and provide any other necessary support services needed by any residents. 
            </Text>

            <Text style={styles.subheading}>4. Mobile phones and other Electronic Devices</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            The use of cell phones and electronic gadgets are not permitted in the
            following places:
            {'\n'}
            (a) Classrooms
            {'\n'}
            (b) Library
            {'\n'}
            (c) Examination Hall
            {'\n'}
            (d) Formal sessions in the auditorium and
            {'\n'}
            (e) Formal meetings
            </Text>
            <Text style={styles.subheading}>5.Academic Block and office</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            All the offices including President, Deans, teaching & nonteaching staff and classrooms are located in the adjacent
            buildings. The office is numbered and name plates attached
            on the doors for any queries and discussions.
            </Text>

            <Text style={styles.subheading}>6.Dress Code</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            Proper national dress code must be observed while you are at
            the academic block, auditorium, dining hall, prayer hall,
            computer labs and libraries. 

            </Text>

            <Text style={styles.subheading}>7.Computing Facilities</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            The College has two main computer labs with 40 computers
            each with internet connection. The labs are designated mainly
            for practical session demanded by different IT modules. The
            students can use the lab for accessing sources for assignment,
            typing assignments, preparing presentation, and so on. 

            </Text>
            <Text style={styles.subheading}>8.Class Tutorial Rooms</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            The College has adequate classrooms designed to accommo
            date 30 to 50 students. The classrooms are furnished with ex
            pensive furniture and graffiti on furniture and walls are strictly
            prohibited.
            </Text>

            <Text style={styles.subheading}>9. Library</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            The Libraries are a vital component of intellectual life on
            campus. The modern academic library is not simply a collection
            of books. Here, we foster scholarly engagement; encourage
            critical inquiry; and celebrate a diversity of experiences, interests
            and thought. In addition to an impressive breadth of books,
            journals, and databases, we also have newspapers, magazines
            and more. It also has a space for study with well-furnished
            facilities. Students can borrow certain numbers of books at a
            time and return it to the library on or before the stipulated due
            dates. The college library is managed by an experienced
            librarians.
            </Text>

            <Text style={styles.subheading}>10. Prayer Sessions</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            All the students are required to attend the prayer session in the
            prayer hall as per the schedule prepared by the Prayer Coordi
            nators. An appropriate fine decided by the committee is levied for
            the absenteeism. The fine collected will be deposited with the
            College Accounts section. This money is used during the
            religious rituals organized in the college.
            </Text>

            <Text style={styles.subheading}>11. Games and Sports Facilities</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            The college has adequate sport facilities like badminton,
            basketball, football and volleyball courts. In order to provide
            wholesome education to students, the college gives importance
            to all games and sports activities.
            </Text>

            <Text style={styles.subheading}>12. Morning Assembly</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            Morning assembly is considered one of the important activities
            of the college. It is the only time where all students and lecturers
            come together for a moment to pray and share thoughts and
            experiences. It is also used as the forum to disseminate
            important information and announcements. Therefore, it is
            compulsory for all the students to participate in it and attendance
            will be taken and credited.
            </Text>

            <Text style={styles.subheading}>13. Consultation and Counseling</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            Every individual should be treated with respect in and around the
            college premises. An individual can appeal against offences
            imposed by another individual group. You may approach to any
            student leaders, Resident Tutorss and deans regarding any
            matter or problems for consultation and support services.
            </Text>

            <Text style={styles.subheading}>14. Outstation Leave</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            Outstation leave shall NOT be granted to any students under
            normal circumstances. Students need to make their own
            assessment and rational decisions whether to attend the classes
            or to stay away. You may take the following guidelines to prevent
            the future problems that may arise out of the leave procedures:
            {'\n'}
            (a) For students who need major medical attention or need
            to attend to direct family members in unavoidable
            conditions, formal leave has to be approved
            BEFORE YOU LEAVE THE CAMPUS by the approving
            authority concerned.
            {'\n'}
            (b) Leave for all project works, field visits and any other
            academic outstation leave after following the due process
            and verification shall be approved by the Dean, Academic
            Affairs.
            {'\n'}
            (c) Non – academic leave shall be approved by Dean of
            Student Affairs during office hours (9.00 am – 5pm).
            Resident Tutor concerned shall approve leave during
            emergency and odd hours (weekends, Sundays and
            government holidays). Leave form must be submitted by
            the Resident Tutors to the office of Dean, Student Affairs.
            {'\n'}
            (d) Medical prescriptions (photocopied) should be submitted
            within three working days of joining the college as
            evidence.
            {'\n'}
            (e) All your processed leave forms are then forwarded to the
            College Academic Committee for computation on
            monthly basis. Any issues arising from the un-processed
            leave forms will not be the responsibility of the leave ap
            proving authority.
            {'\n'}
            (f) OPD slip from the Gyelposhing hospital shall NOT be
            considered for medical leave and class attendance.
            {'\n'}
            (g) Standard Leave forms are available in the book store
            within the college premises
            </Text>
        </ScrollView>
    </View>
  )
}

export default StudentServices

const styles = StyleSheet.create({
    giPage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        alignItems:'center',
        alignContent:'center',
        },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:20,
       },
    scrollContainer:{
        marginTop:10,
        borderWidth:1,
        width:350,
        borderRadius:10,
        height:600,
        borderColor:'blue'
    },
    heading:{
        fontFamily:'serif',
        fontSize:20,
        fontWeight:'bold',
        padding:10,
    },
    subheading:{
        fontFamily:'serif',
        fontSize:16,
        fontWeight:'bold',
        padding:10,
        lineHeight:30
    }
})