import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'
import React from 'react'

const AcademicInfo = () => {
  return (
    <View style={styles.giPage}>
      <View style={styles.gcit}>
         <Image
             style={styles.gcit}
             source={require('../../images/gcit.logo.png')}
         />
     </View>
     <View style={{alignItems:'center'}}><Text style={styles.heading}>Academic Information</Text></View>
     
     <ScrollView style={styles.scrollContainer}>
            {/* <View style={styles.scrollContainer}> */}
            <Text style={styles.subheading}>1. Assessment and Progression Rule</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            To pass a module a student must obtain a minimum of
            50% overall including both the continuous assessment and se
            mester end examination. In addition, students must obtain a
            minimum of 40% each in continuous assessment and semester
            end examinations
            
            </Text>

            <Text style={styles.subheading}>2. Re-assessment and Repeat of a module</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            •  Reassessment is permitted to allow a student to make good an
                initial failure. It thus affords the student an opportunity to succeed
                in the failed component of a module (s) (coursework or end of
                semester examination) and ultimately gain an award
            {'\n'}
            •  The Board of Examiners shall decide on the form of the re-as
                sessment (e.g. written examination, viva voce, or an additional
                assignment, or any additional requirement which was not met),
                taking cognizance of the nature of the failed module and the
                nature of the failure. This may differ from the format of the first
                assessment and need not be the same for all students.
            {'\n'}
            •  A student may be re-assessed in a failed module(s) provided that
                he or she:
                {'\n'}
                (a) has not failed in more than 30% of the total number of modules
                prescribed for that semester (rounded off to the nearest whole
                number of modules).
                {'\n'}
                (b) shall not be re-assessed in a module more than once.
                {'\n'}
                (c) Re-assessments should take place before, or at the
                commencement of the next academic semester.
                {'\n'}
            •    A student who is re-assessed for a module failure, where there are
                no clear extenuating circumstances , shall be awarded no more
                than 50% on passing the re-assessment, this being the
                minimum pass mark.
                {'\n'}
            
            •   A student shall be eligible to repeat failed module(s) where he
                or she:
                {'\n'}
                (a) has failed in the re-assessment of a module(s). In such an
                event, the student shall meet all assessment requirements
                of those modules. For students under this category, attendance
                in lectures is NOT mandatory.
                {'\n'}
                (b) has failed more than 30% of the total number of modules
                prescribed for that semester (rounded off to the nearest whole
                number of modules). In such an event the student shall meet all
                teaching, learning and assessment requirements of the failed
                modules. For students under this category, attendance in
                lectures is mandatory.
                {'\n'}
            </Text>

            <Text style={styles.subheading}>3. Award of Grades</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            •  On completion of the programme, the following grades will be
            awarded to the successful candidates:
            {'\n'}
            An outstanding performance for 80% & above
            {'\n'}
            Very good performance for 70% - 79.9%
            {'\n'}
            Average performance for 60% - 69.9%
            {'\n'}
            Satisfactory performance for 50% - 59.9%
            {'\n'}
            Fail Below 49.9%
            </Text>

            <Text style={styles.subheading}>4. Requirements for Graduation</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            • Student has at least secured 50% in academic
            performance.{'\n'}
            • Student has passed/ fulfilled specific programme
            requirements.{'\n'}
            • Student has no pending disciplinary action in the
            college, RUB, the society and the country as per the
            discretion of concerned authority.{'\n'}
            • The award is recommended by a Board of Examiners
            convened, constituted and acting under regulations
            approved by the Academic Board{'\n'}
            • Student is a registered student of the University at the
            time of his or her assessment and has fulfilled all
            financial obligations to the University
            </Text>
        </ScrollView>
    </View>
  )
}

export default AcademicInfo

const styles = StyleSheet.create({
    giPage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        alignItems:'center',
        alignContent:'center',
        },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:20,
       },
    scrollContainer:{
        marginTop:10,
        borderWidth:1,
        width:350,
        borderRadius:10,
        height:600,
        borderColor:'blue'
    },
    heading:{
        fontFamily:'serif',
        fontSize:20,
        fontWeight:'bold',
        padding:10,
    },
    subheading:{
        fontFamily:'serif',
        fontSize:16,
        fontWeight:'bold',
        padding:10,
        lineHeight:30
    }
})