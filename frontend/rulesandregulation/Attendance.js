import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'
import React from 'react'

const Attendance = () => {
  return (
    <View style={styles.giPage}>
      <View style={styles.gcit}>
         <Image
             style={styles.gcit}
             source={require('../../images/gcit.logo.png')}
         />
     </View>
     <View style={{alignItems:'center'}}><Text style={styles.heading}>Attendance</Text></View>
     
     <ScrollView style={styles.scrollContainer}>
            {/* <View style={styles.scrollContainer}> */}
            <Text style={styles.subheading}></Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            Attendance for classes is expected to be 100%. However, due
            to extenuating circumstances:
            {'\n'}
            • A student must meet minimum of 90% attendance for
            each module calculated on the contact time for each
            module;
            {'\n'}
            • 80% is set as the minimum attendance requirement to
            account for extenuating circumstances and other
            assignments of the college or University, beyond which
            no other arrangements will be made;
            {'\n'}
            • Failure to meet this requirement shall result in failure of
            a module(s) and the students shall repeat the module
            again with payment when it is offered next.
            
            </Text>

           
        </ScrollView>
    </View>
  )
}

export default Attendance

const styles = StyleSheet.create({
    giPage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        alignItems:'center',
        alignContent:'center',
        },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:20,
       },
    scrollContainer:{
        marginTop:10,
        borderWidth:1,
        width:350,
        borderRadius:10,
        height:600,
        borderColor:'blue'
    },
    heading:{
        fontFamily:'serif',
        fontSize:20,
        fontWeight:'bold',
        padding:10,
    },
    subheading:{
        fontFamily:'serif',
        fontSize:16,
        fontWeight:'bold',
        padding:10,
        lineHeight:30
    }
})