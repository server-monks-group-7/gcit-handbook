import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'
import React from 'react'

const GeneralInfo = () => {
  return (
    <View style={styles.giPage}>
      <View style={styles.gcit}>
         <Image
             style={styles.gcit}
             source={require('../../images/gcit.logo.png')}
         />
     </View>
     <View style={{alignItems:'center'}}><Text style={styles.heading}>General Information</Text></View>
     
     <ScrollView style={styles.scrollContainer}>
            {/* <View style={styles.scrollContainer}> */}
            <Text style={styles.subheading}>1. Rights and Responsibility</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
            
           <Text style={{fontWeight:'bold'}}> You have right to :</Text>
            {'\n'}
            •  expect an education of the highest quality;
            {'\n'}
            •  develop potential to the best of your ability;
            {'\n'}
            •  inquire about and to recommend improvements in policies,
            regulations, and procedures affecting the welfare of students.
            {'\n'}
            •  a campus environment that is characterized by safety and
            order; and
            {'\n'}
            •  a fair and equal opportunities in your journey towards excellence. 
            {'\n'}
            {'\n'}
           <Text style={{fontWeight:'bold'}}>You have an obligation:</Text>
            {'\n'}
            •  to be fully acquainted with published regulations and to comply
            with them in the interest of an orderly and productive community;
            {'\n'}
            •  of knowing that one’s conduct reflects not only upon self but
            also upon the institution;
            {'\n'}
            •  to follow the tenets of common decency and acceptable behavior
            commensurate with the aspiration implied by a college
            education; and
            {'\n'}
            •  to respect the rights and property of others
            
            </Text>
            <Text style={styles.subheading}>2. VISION and MISSION</Text>
            <Text style={{fontFamily:'serif',lineHeight:30,padding:10}}>
                <Text style={{fontWeight:'bold'}}>VISION</Text>
                {'\n'}
                A Centre of Excellence in ICT education steeped in Values
                and Ethics 
                {'\n'}
                {'\n'}
                <Text style={{fontWeight:'bold'}}>MISSION</Text>
                {'\n'}
                • To provide ICT education at tertiary level, of relevance and
                quality to fulfill the needs of the society.
                {'\n'}
                • To carryout research to generated new knowledge and promote
                innovation in the area of Information and Communication
                Technology.
                {'\n'}
                • To play distinctive role in ICT education through learning by
                doing
                {'\n'}
                • To provide training and professional services for continuous
                enhancement of knowledge, capacity building and community
                development.
                {'\n'}
                • To prepare the students at the forefront of IT knowledge to face
                challenges of emerging technologies
            </Text>
        </ScrollView>
    </View>
  )
}

export default GeneralInfo

const styles = StyleSheet.create({
    giPage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        alignItems:'center',
        alignContent:'center',
        },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:20,
       },
    scrollContainer:{
        marginTop:10,
        borderWidth:1,
        width:350,
        borderRadius:10,
        height:600,
        borderColor:'blue'
    },
    heading:{
        fontFamily:'serif',
        fontSize:20,
        fontWeight:'bold',
        padding:10,
    },
    subheading:{
        fontFamily:'serif',
        fontSize:16,
        fontWeight:'bold',
        padding:10,
        lineHeight:30
    }
})