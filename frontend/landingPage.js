import React from "react";
import { Button, StyleSheet, Text, TextInput, View, Pressable,Image,TouchableOpacity} from 'react-native';
// import { NavigationContainer } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";

const LandingPage = () => {
  const navigation=useNavigation();
  return (
    <View style={styles.homePage}>
       <View style={styles.gcit}>
            <Image
                style={styles.gcit}
                source={require('../images/gcit.logo.png')}
            />
        </View>
      <View style={styles.box}>
        <TouchableOpacity onPress={()=>navigation.navigate('Home Screen')}>
          <Text style={styles.home}>HOME</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.box}>
        <TouchableOpacity onPress={()=>navigation.navigate('Map')}>
          <Text style={styles.map}> MAP</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.box}>
      <TouchableOpacity onPress={()=>navigation.navigate('Time Table')}>
          <Text style={styles.timetable}>TIMETABLE</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.box}>
      <TouchableOpacity onPress={()=>navigation.navigate('Rules')}>
          <Text style={styles.collegeRules}>RULES</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.flexWrapperFive}>
      <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
          <Text style={styles.adminLogin}>Admin Login</Text>
        </TouchableOpacity>
        <Image
            style={styles.admin1}
            source={require('../images/admin.png')}
      />
      </View>
    </View>
  );
};
const styles=StyleSheet.create({
   homePage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center',
        },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:50,
       },
    
    box:{
      backgroundColor:'rgba(44,126,249,1)',
        margin:20,
        borderRadius:15,
        padding:10,
        width:330,
        display:'flex',
        alignItems:'center',
        borderColor:'black',
    },

    home:{
        height:70,
        width:271,
        fontFamily:'serif',
        fontSize:32,
        paddingLeft:77,
        paddingTop:17,
        // fontWeight:400,
        alignItems:'center',

        
       },

    map:{
        height:68,
        width:243,
        fontFamily:'serif',
        paddingLeft:80,
        paddingTop:17,
        fontSize:32,
        // fontWeight:400,
        display:'flex',
        alignItems:'center',
        },
    timetable:{
        height:68,
        width:243,
        fontFamily:'serif',
        paddingLeft:30,
        paddingTop:17,
        fontSize:32,
        // fontWeight:400,
        display:'flex',
        alignItems:'center',
    },

    collegeRules:{
        height:68,
        width:243,
        fontFamily:'serif',
        fontSize:32,
        paddingTop:17,
        paddingLeft:70,
        // fontWeight:400,
        display:'flex',
        alignItems:'center',
        },

    flexWrapperFive:{
        
        marginTop:20,
        display:'flex',
        flexDirection:'row',
        },

    adminLogin:{
        height:44,
        width:155,
        fontSize:20,
        fontFamily:'serif',
        // fontWeight:700,
        display:'flex',
        alignItems:'center',
        marginTop:15,
       
    },

    admin1:{
       width:50,
       height:50,
    }
});

export default LandingPage;