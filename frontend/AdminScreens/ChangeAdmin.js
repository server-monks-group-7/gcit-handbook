import React, { useEffect, useState } from "react";
// import { Link } from "react-router-dom"
import './landingPage.css';
import { auth } from '../config';
// import LandingPage from "./LandingPage";
import { useNavigate } from "react-router-dom";

const ChangeAdmin=()=>{
    const navigate=useNavigate()
    const [email, setEmail] =useState('');
    const [password, setPassword] =useState('');
    const handleAdminChange=()=>{
        auth
        .createUserWithEmailAndPassword(email,password)
        .then(userCredentials=>{
            const user=userCredentials.user;
            console.log('Registration Succesful for : ',user.email);
            alert('Registration Successful for', user.email)
            navigate("/")
        })
        .catch(error => alert(error.message))
    }
    return(
        
        <div style={{alignItems:'center', display:'flex', flexDirection:'column'}}>
            <div style={{backgroundColor:'gray', marginRight:'82%', marginTop:20}}><img src='https://www.gcit.edu.bt/2022/wp-content/uploads/sites/3/2021/12/footer_logo.png' alt="Loading Please Wait" style={{height:100, width:100}}/></div>
            <h1>New Admin</h1>
            <input 
                placeholder="Email" 
                datatype="email" 
                className="inputBox" 
                value={email} 
                onChange={Email => setEmail(Email.target.value)}/>

            <input 
                placeholder="Password" 
                type="password"
                className="inputBox" 
                secureTextEntry
                value={password} onChange={Password=>setPassword(Password.target.value)}/>

            <button className="LoginButton" onClick={handleAdminChange}><a className="LoginText">Add</a></button>
        </div>
    )
}

export default ChangeAdmin