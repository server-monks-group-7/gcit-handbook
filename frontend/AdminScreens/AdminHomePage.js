import { StyleSheet, Text, View,Image,TouchableOpacity, SafeAreaView,Alert } from 'react-native'
import React, {useState} from 'react'
import * as ImagePicker from 'expo-image-picker';
import {firebase} from '../../firebaseconfig';

const AdminHomePage = () => {
    const [image, setImage] =useState(null);
    const [uploading, setUploading]= useState(false);
    
    const pickImage=async()=>{
        //no permisson request is necessary for launching the libaray
        let result=await ImagePicker.launchImageLibraryAsync({
            allowsEditing:true,
            aspect:[4,3],
            quality:1,
        });

        const source={uri:result.uri};
        console.log(source);
        setImage(source);
    };

    const uploadImage=async ()=>{
        setUploading(true);
        const response =await fetch(image.uri);
        const blob= await response.blob();
        const filename=image.uri.substring(image.uri.lastIndexOf('/')+1);
        var ref=firebase.storage().ref().child(filename).put(blob);

        try{
            await ref;
        }catch(e){
            console.log(e);
        }
        setUploading(false);
        Alert.alert(
            'Photo uploaded '
        );
        setImage(null);
    };
  return (
    <View style={styles.AdminHomePage}>
        <View style={styles.gcit}>
         <Image
             style={styles.gcit}
             source={require('../../images/gcit.logo.png')}
         />
     </View>
      <Text style={styles.heading}>Home</Text>
      <TouchableOpacity style={styles.selectButton} onPress={pickImage}>
        <Text style={styles.buttonText}>Pick an Image</Text>
      </TouchableOpacity>

      <View style={styles.imageContainer}>
        {image && <Image source={{uri:image.uri}} style={{width:300,height:300}}/>}
        <TouchableOpacity
            style={styles.uploadButton}
            onPress={uploadImage}
        >
        <Text style={styles.buttonText}>Upload Image</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default AdminHomePage;

const styles = StyleSheet.create({
    AdminHomePage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center',
        },
    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
    },
    heading:{
        fontFamily:'serif',
        fontSize:32,
        fontWeight:'bold',
        marginTop:20,
        marginBottom:10,
    },
    selectButton:{
        borderRadius:10,
        width:150,
        height:50,
        backgroundColor:'blue',
        alignItems:'center',
        justifyContent:'center',
        marginTop:30,
    },
    uploadButton:{
        borderRadius:10,
        width:150,
        height:50,
        backgroundColor:'blue',
        alignItems:'center',
        justifyContent:'center',
        marginTop:30,
    },
    buttonText:{
        color:'white',
        fontSize:18,
        fontWeight:'bold',
    },
    imageContainer:{
        margitnTop:30,
        marginBottom:50,
        alignItems:'center',
    }
})
