import { StyleSheet, Text, View,Image, Button } from 'react-native'
import React from 'react'
import { auth } from '../../firebaseconfig'
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const AdminLandingPage = () => {
    const navigation=useNavigation();

    const handleSignOut=()=>{
        auth
        .signOut()
        .then(()=>{
            navigation.replace("Login")
        })
    }
  return (
    <View style={styles.homePage}>
    <View style={styles.gcit}>
         <Image
             style={styles.gcit}
             source={require('../../images/gcit.logo.png')}
         />
     </View>
   <View style={styles.box}>
     <TouchableOpacity onPress={()=>navigation.navigate('Admin Home')}>
       <Text style={styles.home}>HOME</Text>
     </TouchableOpacity>
   </View>
   <View style={styles.box}>
     <TouchableOpacity onPress={()=>navigation.navigate('Map')}>
       <Text style={styles.map}> MAP</Text>
     </TouchableOpacity>
   </View>
   <View style={styles.box}>
   <TouchableOpacity onPress={()=>navigation.navigate('Time Table')}>
       <Text style={styles.timetable}>TIMETABLE</Text>
     </TouchableOpacity>
   </View>
   <View style={styles.box}>
   <TouchableOpacity onPress={()=>navigation.navigate('Map')}>
       <Text style={styles.collegeRules}>RULES</Text>
     </TouchableOpacity>
   </View>
   <View style={styles.box}>
    <TouchableOpacity
        onPress={handleSignOut}
    >
        <Text style={{color:'white',fontSize:16, fontWeight:'bold'}}>Sign Out</Text>
    </TouchableOpacity>

   </View>
 </View>
    
  )
}

export default AdminLandingPage

const styles = StyleSheet.create({
    homePage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center',
        },
    box:{
        backgroundColor:'purple',
        margin:20,
        borderRadius:15,
        padding:10,
        width:330,
        display:'flex',
        alignItems:'center',
        borderColor:'black',
    },

    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:40,
       },

    home:{
        height:70,
        width:271,
        fontFamily:'serif',
        fontSize:32,
        paddingLeft:77,
        paddingTop:17,
        // fontWeight:400,
        alignItems:'center',
        color:'white'

        
       },

    map:{
        height:68,
        width:243,
        fontFamily:'serif',
        paddingLeft:80,
        paddingTop:17,
        fontSize:32,
        // fontWeight:400,
        display:'flex',
        alignItems:'center',
        color:'white'
        },

    timetable:{
        height:68,
        width:243,
        fontFamily:'serif',
        paddingLeft:30,
        paddingTop:17,
        fontSize:32,
        // fontWeight:400,
        display:'flex',
        alignItems:'center',
        color:'white'
    },
        

    collegeRules:{
        height:68,
        width:243,
        fontFamily:'serif',
        fontSize:32,
        paddingTop:17,
        paddingLeft:70,
        // fontWeight:400,
        display:'flex',
        alignItems:'center',
        color:"white"
        },

})