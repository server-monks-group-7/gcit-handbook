import React, { useEffect, useState } from "react";
import { Text,View,Image,StyleSheet,TextInput,TouchableOpacity} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { auth } from "../../firebaseconfig";

const AdminLogin=()=>{
    const navigation=useNavigation();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    useEffect(() => {
        const unsubscribe=auth.onAuthStateChanged(user =>{
            if (user){
                navigation.navigate("Admin")
            }
        })
        return unsubscribe
    },[])
    
    const handleLogin=()=>{
        auth.signInWithEmailAndPassword(email,password)
        .then(userCredentials =>{
            const user=userCredentials.user;
            console.log('Logged in with :', user.email);
        })
        .catch(error => alert(error.message))
    }
    return(
        <View style={styles.loginPage}>
        <View style={styles.gcit}>
            <Image
                style={styles.gcit}
                source={require('../../images/gcit.logo.jpg')}
            />
        </View>
        <Text style={styles.loginText}>Login</Text>
        <View style={styles.secondContainer}>
            <TextInput 
                 style={styles.inputBox}
                 placeholder='Email' 
                 value={email}
                 onChangeText={Email => setEmail(Email)}
                 ></TextInput>
            <TextInput 
                style={styles.inputBox} 
                placeholder='Password' 
                value={password}
                onChangeText={Password => setPassword(Password)}
                secureTextEntry
                />
            <TouchableOpacity 
                style={styles.loginButton} 
                onPress={handleLogin}>
                    <Text style={styles.buttonText}>Login</Text></TouchableOpacity>
        </View>
        </View>
        

    )
}

const styles=StyleSheet.create({
    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:3,
    },
    loginText:{
        marginTop:120,
        fontSize:34,
        fontFamily:'serif'
    },  
    loginPage:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        marginTop:-10,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center',
   },
   secondContainer:{
    alignItems:'center',
    marginTop:30,
   },
   inputBox:{
    borderColor:'black',
    borderRadius:10,
    borderWidth:1,
    width:300,
    height:50,
    margin:20,
    fontSize:18,
    paddingLeft:10,
   },
   buttonText:{
    fontFamily:'serif',
    fontSize:24,
    padding:10,
    
   },
   loginButton:{
    backgroundColor:'rgba(44,126,249,1)',
    borderRadius:10,
   },


});


export default AdminLogin;