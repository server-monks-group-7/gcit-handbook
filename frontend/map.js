import React from "react";
import { Text,Image, StyleSheet, View } from "react-native";

const Map=() =>{
    return(
        <View style={styles.mapPage}>
            <View style={styles.gcit}>
                <Image
                    style={styles.gcit}
                    source={require('../images/gcit.logo.jpg')}
                />
            </View>

            <Text style={styles.heading}>
                Campus Map
            </Text>  
            <Image
                style={styles.image}
                source={require('../images/campusMap.jpg')}
            />  
        </View>
    )
}

const styles=StyleSheet.create({
        gcit:{
            width: 368,
            height: 65,
            marginTop:5,
            marginLeft:0,
        },
        mapPage:{
            // backgroundColor : 'rgba(58, 54, 54,1)',
            display:'flex',
            flexDirection:'column',
            alignItems:'flex-start',
            // borderColor:'black',
            // paddingTop:20,
            marginTop:-10,
            paddingRight:5,
            paddingBottom:51,
            alignItems:'center',
            alignContent:'center',
            },
        heading:{
            marginTop:40,
            fontSize:28,
        },
        image:{
            marginTop:20,
            width:350,
            height:550,
        }
        
});

export default Map;