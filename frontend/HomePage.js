import { StyleSheet, Text, View, Image, SafeAreaView, ScrollView } from 'react-native'
import React, {useState, useEffect} from 'react'
import {firebase} from '../firebaseconfig'
const HomePage = () => {
    // const [imageTab, setImageTab] = useState([]);
    // firebase.storage()
    //     .ref()
    //     .listAll()
    //     .then(function(result) {
    //         result.items.forEach(function(imageRef) {
    //             imageRef.getDownloadURL().then(function(url) {
    //                 imageTab.push(url);
    //                 console.log(imageTab);
    //                 setImageTab(imageTab);
    //                 console.log(imageTab[0])
    //             }).catch(function(error) {
    //                 // Handle any errors
    //             });
    //         });
    //     })
    //     .catch((e) => console.log('Errors while downloading => ', e));
    
  return (
    
    <View style={styles.homeContainer}>
        <View style={styles.gcit}>
            <Image
                style={styles.gcit}
                source={require('../images/gcit.logo.png')}
            />
        </View>

      <Text style={styles.heading}>GCIT</Text>
    <View style={styles.imageContainer}>
        {/* {imageTab.map(i => (<Image style={{height: 300, width: 350, borderRadius:5}} source={{uri: i}} />))} */}
        {/* <Image source={{uri : imageTab[0]}} style={styles.homepageImage}/> */}
        <Image source={{uri : 'https://firebasestorage.googleapis.com/v0/b/gcit-handbook.appspot.com/o/f1fc3971-97dc-4478-b499-624b51efcfd6.jpeg?alt=media&token=c87553a1-5404-404b-bc06-2fb1dc42465d'}} 
                style={{height:300, width:350, borderRadius:5}}
        />
    </View>

    <SafeAreaView style={styles.introContainer}>
        <ScrollView>
            <Text style={styles.introHeading}>Background</Text>
            <Text style={styles.content}>
            Gyalposhing College of Information Technology(GCIT), officially inaugurated as one of the integral colleges of the Royal
            Unitversity of Bhutan on 6th October 2017. The college is established in Eastern Bhutan with the VISION to be the center
            of Excellence in Information Technology steeped in GNH values. It has a mission to accelerate Bhutan's ICT Talent Development.
            As such, the college has new education curriculum offerings taht are designed to cater to IR.0 and all its emerging techologies.
            {'\n'} {'\n'}
            The colelge will  have two anchoring schools. School of Digital Media and Development focuses on Rapid Prototyping capability, a finesse in UX/UI design and the overall spectrum of digital media design. 
            Technopreneruship skills in this program will support through leadership on Go-To Market strategy for the product and services.
            While School of Computing offers three cutting edge core specializations, namely, AI and Data Science Development, Blockchain Development
            and Full Stack Development. Each specializations will ahve skills to capitalize on Data Insights, to develop Blockchain solutions
            with greater security and privacy, and to deploy product and services with greater agility, scalability and reliability.
            {'\n'} {'\n'}

            The program is taught by qualified and dedicated local and expatriate faculty members. The methodology of "Inquiry-based learning"
             is a unique one adopted by GCIT. It aims at giving hands-on experience which help the students understand the practial 
             implementations aspects and the concepts associated with it.
             {'\n'}
            </Text>
        </ScrollView>
    </SafeAreaView>

     </View>
  )
}

export default HomePage

const styles = StyleSheet.create({
    homeContainer:{
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center',
    },
    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:0,
        marginBottom:20,
    },
    heading:{
        fontSize:32,
        fontFamily:'serif',
        fontWeight:'bold',
        marginBottom:10,
    },
    introContainer:{
        width :310,
        height: 280,
        // borderWidth:1,
        borderRadius:10,
        marginTop:15,
    },
    introHeading:{
        paddingLeft:'35%',
        fontFamily:'serif',
        fontSize:16,
        fontWeight:'bold',
        marginBottom:10
    },
    content:{
        fontFamily:'serif',
        lineHeight:25,
    }
})