import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'
import React from 'react'

const Blockchain = () => {
  return (
    <View style={styles.AIandDSpage}>
      <View style={styles.gcit}>
            <Image
                style={styles.gcit}
                source={require('../../images/gcit.logo.jpg')}
            />
        </View>
        <ScrollView
            horizontal={true}
            style={styles.mainContainer}
        >
            <View style={styles.dayOfWeek}>
               <View style={styles.headingContainer}><Text style={styles.dow}>Monday</Text></View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 9:00 - 10:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 10:00 - 11:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 11:15 AM - 12:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 12:15 - 01:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 02:15 - 03:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 03:15 - 04:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 04:15 - 05:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
               
            </View>
            <View style={styles.dayOfWeek}>
                <View style={styles.headingContainer}><Text style={styles.dow}>Tuesday</Text></View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 9:00 - 10:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 10:00 - 11:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 11:15 AM - 12:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 12:15 - 01:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 02:15 - 03:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 03:15 - 04:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 04:15 - 05:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    
                
            </View>
            <View style={styles.dayOfWeek}>
                <View style={styles.headingContainer}><Text style={styles.dow}>Wednesday</Text></View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 9:00 - 10:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 10:00 - 11:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 11:15 AM - 12:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 12:15 - 01:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 02:15 - 03:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 03:15 - 04:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 04:15 - 05:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
               
                
            </View>
            <View style={styles.dayOfWeek}>
                <View style={styles.headingContainer}><Text style={styles.dow}>Thursday</Text></View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 9:00 - 10:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 10:00 - 11:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 11:15 AM - 12:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 12:15 - 01:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 02:15 - 03:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 03:15 - 04:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 04:15 - 05:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    
                    
                </View>
            </View>
            <View style={styles.dayOfWeek}>
                <View style={styles.headingContainer}><Text style={styles.dow}>Friday</Text></View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 9:00 - 10:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 10:00 - 11:00 AM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 11:15 AM - 12:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 12:15 - 01:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 02:15 - 03:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 03:15 - 04:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
                    <View style={styles.period}>
                            <Text style={styles.time}>Time: 04:15 - 05:15 PM</Text>
                            <Text style={styles.text}>Module: </Text>
                            <Text style={styles.text}>Tutor: </Text>
                    </View>
               
               
            </View>
            


        </ScrollView>
    </View>
  )
}

export default Blockchain

const styles = StyleSheet.create({
    AIandDS:{
        // backgroundColor : 'rgba(58, 54, 54,1)',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        // borderColor:'black',
        // paddingTop:20,
        marginTop:-10,
        paddingRight:5,
        paddingBottom:51,
        alignItems:'center',
        alignContent:'center'
    },
    gcit:{
        width: 368,
        height: 65,
        marginTop:5,
        marginLeft:5,
    },
    mainContainer:{
        display:'flex',
        flexDirection:'row',
        marginTop:100,
    },
    headingContainer:{
        backgroundColor:'green', 
        width:169, 
        alignItems:'center',
    },
    period:{
        width:169,
        height:60,
        borderWidth:1,
        alignItems:'center',
    },
    time:{
        fontFamily:'serif',
        fontSize:10,
    },
    text:{
        fontFamily:'serif',
        fontSize:12,
    },
    dayOfWeek:{
        width:170,
        borderWidth:1,
        alignItems:'center',
       
    },
    dow:{
        fontFamily:'serif',
        fontWeight:'bold',
        fontSize:18,
        paddingTop:5,
        paddingBottom:5,
        color:'white'
    }
})