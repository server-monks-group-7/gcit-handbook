import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import LandingPage from "./frontend/landingPage";
import HomePage from "./frontend/HomePage";
import Map from "./frontend/map";
import TimeTable from "./frontend/TimeTable";
import Rules from "./frontend/Rules";
//imports for sections of Rules and Regulations
import GeneralInfo from "./frontend/rulesandregulation/GeneralInfo";
import AcademicInfo from "./frontend/rulesandregulation/AcademicInfo";
import ExaminationGuidelines from "./frontend/rulesandregulation/ExaminationGudlines";
import Attendance from "./frontend/rulesandregulation/Attendance";
// Admin pages
import AdminLogin from "./frontend/AdminScreens/adminLogin";
import AdminLandingPage from "./frontend/AdminScreens/AdminLandingPage";
import ChangeAdmin from "./frontend/AdminScreens/ChangeAdmin";
import AdminHomePage from "./frontend/AdminScreens/AdminHomePage";
// import below are for timetable
import AIandDS from "./frontend/TimeTable/AIandDS";
import DigitalMedia from "./frontend/TimeTable/DigitalMedia";
import Blockchain from "./frontend/TimeTable/Blockchain";
import FullStack from "./frontend/TimeTable/FullStack";
import ThirdYearCS from "./frontend/TimeTable/ThirdYearCS";
import FourthYearCS from "./frontend/TimeTable/FourthYearCS";
import ThirdYearIT from "./frontend/TimeTable/ThirdYearIT";
import FourthYearIT from "./frontend/TimeTable/FourthYearIT";
import StudentServices from "./frontend/rulesandregulation/StudentServices";




const Stack=createStackNavigator();

const Screens=()=>{
    return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen
              name="Home"
              component={LandingPage}
            />
            <Stack.Screen
              name="Home Screen"
              component={HomePage}
            />
            <Stack.Screen
              name="Map"
              component={Map}
            />
            <Stack.Screen
              name="Time Table"
              component={TimeTable}
            />
            <Stack.Screen
              name="Rules"
              component={Rules}
            />
        {/* screens for Rules and Regulations Sections */}
          <Stack.Screen
            name="General Information"
            component={GeneralInfo}
          />
          <Stack.Screen
            name="Academic Information"
            component={AcademicInfo}
          />

          <Stack.Screen
            name="Examination Guidelines"
            component={ExaminationGuidelines}
          />
           <Stack.Screen
            name="Attendance"
            component={Attendance}
          />
           <Stack.Screen
            name="Student Services"
            component={StudentServices}
          />
        {/* After this is for Admins Screen */}
            <Stack.Screen
              name="Login"
              component={AdminLogin}
            />

            <Stack.Screen
              name="Edit Admin"
              component={ChangeAdmin}  
            />
            <Stack.Screen
              name="Admin"
              component={AdminLandingPage}
            />
            <Stack.Screen
              name="Admin Home"
              component={AdminHomePage}
            />
            
          {/*Screens below this are for TimeTable of different class  */}
            <Stack.Screen
              name="AI and DS"
              component={AIandDS}
            />
            <Stack.Screen
              name="Digital Media"
              component={DigitalMedia}
            />
            <Stack.Screen
              name="Blockchain"
              component={Blockchain}
            />
            <Stack.Screen
              name="Full Stack"
              component={FullStack}
            />
            <Stack.Screen
              name="Third Year CS"
              component={ThirdYearCS}
            />
            <Stack.Screen
              name="Fourth Year CS"
              component={FourthYearCS}
            />
            <Stack.Screen
              name="Third Year IT"
              component={ThirdYearIT}
            />
            <Stack.Screen
              name="Fourth Year IT"
              component={FourthYearIT}
            />
            </Stack.Navigator>
        </NavigationContainer>
      )
}
export default Screens;