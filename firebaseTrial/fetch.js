import {View, Text, FlatList, StyleSheet, Pressable} from 'react-native';
import React,{useState,useEffect}  from 'react';
import { firebase } from '../firebaseconfig';

const Fetch = () => {

    const [admin, setAdmin] = useState([]);
    const adminRef =firebase.firestore().collection('Admin Info');

    useEffect(async () =>{
        adminRef
        .onSnapshot(
            querySnapshot => {
                const admin=[]
                querySnapshot.forEach((doc) =>{
                    const {Admin_Email,Admin_Password,createdAt} =doc.data()
                    admin.push({
                            id:doc.id,
                            Admin_Email,
                            Admin_Password,
                            
                    })
                })
                setAdmin(admin)
            }
        )
    },[])
    return (
        <View style={{flex:1,marginTop:100}}>
            <FlatList
                style={{height:'100%'}}
                data={admin}
                numColumns={1}
                renderItem={({item}) => (
                    <Pressable style={styles.container}>
                        
                        <View style={styles.innerContainer}>
                            <Text style={styles.itemHeading}>{item.Admin_Email}</Text>
                            <Text style={styles.itemHeading}>{item.Admin_Password}</Text>
                        </View>
                    </Pressable>
                )}
            />
        </View>
    )
}

const styles=StyleSheet.create({
    container:{
        backgroundColor:'#e5e5e5',
        padding:15,
        borderRadius:15,
        margin:5,
        marginHorizontal:10,
    },
    innerContainer:{
        alignContent:'center',
        flexDirection:'column',
    },
    itemHeading:{
        fontWeight:'bold',
    },
    itemText:{
        fontWeight:'300'
    }
})
export default Fetch;
